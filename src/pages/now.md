---
layout: ../layouts/AboutLayout.astro
title: "Now"
---

Hi there! I'm currently a graduate student at Santa Clara University, where I'm pursuing a degree in Computer Science. My journey in the tech world has been an exciting one, filled with learning and growth. I have a keen interest in DevOps and cloud computing, and I particularly enjoy creating infrastructure that supports robust and scalable applications.

## My Tech Stack

- **Java & JavaScript**
- **Python**
- **Linux**
- **Swift, SwiftUI, UIKit**
- **Golang**
- **Amazon Web Services (AWS)**
  - **API Gateway, DynamoDB, Lambda, S3, RDS, EC2, Fargate, IAM**
- **Microsoft Azure**
  - **Function Apps, PowerShell, Blob Storage, CosmosDB, Azure DevOps**
- **Jenkins**

While I've gathered a lot of experience in full-stack development, infrastructure and cloud computing always seemed daunting. However, I've decided to face this challenge head-on and delve deep into these areas. I have several exciting projects lined up, where I plan to showcase my skills and knowledge—so stay tuned!

## This Summer’s Learning Goals

- **Kubernetes (K8s)**: I'm planning to master container orchestration.
- **Advanced CI/CD Pipelines**: To streamline and automate the deployment process even further.
- **Reinforcement Learning**: Exploring the fascinating world of AI and machine learning.

I'm excited about the future and look forward to sharing my journey with you. Whether it's building robust infrastructure, creating dynamic web applications, or diving into the latest technologies, I'm always eager to learn and grow. Keep an eye on my portfolio for updates on my latest projects and developments!



_Last updated: May 27, 2024._